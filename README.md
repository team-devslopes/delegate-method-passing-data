# Slope Session - Passing Data Between ViewControllers using the Delegate Method #

### Description ###

This is the source code accompanying the Passing Data Between ViewControllers Delegate Method Slope Session. The code contains a data sending protocol called DataSentDelegate and sending and receiving ViewControllers which handle passing data through DataSentDelegate.

Happy coding!

![170.png](https://bitbucket.org/repo/9LeroX/images/1216100977-170.png)

**Caleb Stultz**